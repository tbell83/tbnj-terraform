FROM alpine:latest

RUN apk add --no-cache aws-cli bash curl git

RUN git clone --depth=1 https://github.com/tfutils/tfenv.git /root/.tfenv
RUN ln -s /root/.tfenv/bin/* /bin
RUN tfenv install latest

RUN git clone --depth=1 https://github.com/cunymatthieu/tgenv.git /root/.tgenv
RUN ln -s /root/.tgenv/bin/* /bin
RUN tgenv install latest

SHELL [ "/bin/bash" ]
