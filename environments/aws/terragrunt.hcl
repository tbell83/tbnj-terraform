include "globals" {
  path   = "../_env/globals.hcl"
  expose = true
}

terraform {
  source = "${include.globals.locals.repo_root}//terraform/aws"
}

inputs = {
  s3_subdomains = [
    "assets",
  ]
}
