terraform {
  extra_arguments "plan_output" {
    commands = ["plan"]
    arguments = [
      "-out",
      "${get_parent_terragrunt_dir()}/${path_relative_to_include()}/tfplan",
    ]
  }

  extra_arguments "show_plan" {
    commands  = ["show"]
    arguments = ["-no-color"]
  }
}

remote_state {
  backend = "s3"

  config = {
    bucket       = "${local.env}.terraform.${local.region}"
    encrypt      = true
    key          = "${local.git_repo}/${local.repo_relative_path}/terraform.tfstate"
    profile      = local.aws_profile
    region       = local.region
    session_name = "terraform"
  }
}

locals {
  aws_profile        = get_env("CI", "false") == "false" ? "tbnj" : null
  domain             = "tombell.io"
  env                = "tbnj-prod"
  env_vars           = read_terragrunt_config(find_in_parent_folders("env.hcl", "env.hcl"), { locals = {} })
  environment        = "prod"
  git_org            = get_env("CI_PROJECT_ROOT_NAMESPACE", run_cmd("--terragrunt-quiet", "${get_path_to_repo_root()}/.scripts/repo_name.sh", "org"))
  git_repo           = get_env("CI_PROJECT_NAME", run_cmd("--terragrunt-quiet", "${get_path_to_repo_root()}/.scripts/repo_name.sh", "name"))
  region             = "us-east-1"
  repo_relative_path = get_path_from_repo_root()
  repo_root          = get_path_to_repo_root()
}

inputs = {
  aws_profile = local.aws_profile
  domain      = local.domain
  env         = local.env
  environment = local.environment
  region      = local.region
  git_org     = local.git_org
  git_repo    = local.git_repo
}
