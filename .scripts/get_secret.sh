#!/bin/sh

aws --profile "$1" \
	--region "$2" \
	secretsmanager get-secret-value \
	--secret-id "$3" \
	--query SecretString \
	--output text
