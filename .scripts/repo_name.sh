#!/bin/sh

ORIGIN_URL=$(git config remote.origin.url | awk -F':' '{print $NF}' | sed 's/.git//g')
ORG=$(echo "$ORIGIN_URL" | awk -F '/' '{print $1}')
NAME=$(echo "$ORIGIN_URL" | awk -F '/' '{print $2}')

if [ "$1" = "name" ]; then
	echo "$NAME"
elif [ "$1" = "org" ]; then
	echo "$ORG"
else
	echo "$ORIGIN_URL"
fi
