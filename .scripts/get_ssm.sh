#!/bin/sh

aws --profile "$1" \
	--region "$2" \
	ssm get-parameter \
	--name "$3" \
	--with-decryption \
	--query Parameter.Value \
	--output text
