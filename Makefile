PROFILE=tbnj
STATE_BUCKET=tbnj-prod.terraform.us-east-1

.PHONY: *

default: login

login:
	aws --profile ${PROFILE} sso login

clean:
	@find -E environments -regex ".*(\.tfstate.*|crash\.log|\.terraform\.lock\.hcl|tfplan)" -not -path "*/.terragrunt-cache*" -delete
	@find environments -name .terragrunt-cache -type d -exec rm -rf {} \;

fmt:
	@printf "==========\nformatting\n==========\n"
	@terraform fmt -recursive
	@terragrunt hclfmt --terragrunt-hclfmt-file environments/_env/globals.hcl
	@find . -type f -name "terragrunt.hcl" -not -path "*/.terragrunt-cache*" -exec terragrunt hclfmt --terragrunt-hclfmt-file {} \;
	@find . -type f -name "*.auto.tfvars" -not -path "*/.terragrunt-cache*" -exec terraform fmt {} \;
	@find -E . -regex ".*\.(tf|tfvars|hcl)" -not -path "*/.terragrunt-cache*" -exec bash -c 'tail -c1 {} | read -r _ || echo >> {}' \;

lint:
	@printf "======\ntflint\n======\n"
	@tflint --init
	@for DIR in modules terraform; do \
		for SUBDIR in $$(find "$$DIR" -type d -maxdepth 1 -mindepth 1); do \
			echo $$SUBDIR; \
			tflint --format compact "$$SUBDIR"; \
		done; \
	done

latest-aws:
	@echo $(shell curl -s https://raw.githubusercontent.com/hashicorp/terraform-provider-aws/main/CHANGELOG.md | grep -E '^## ([0-9]*\.[0-9]*\.[0-9])' | awk '!/Unreleased/ {print $$2}' | head -n 1)

latest-terra:
	@echo "terragrunt:   $(shell tgenv list-remote | head -n 1)"
	@echo "terraform:    $(shell tfenv list-remote | grep -E -v '(alpha|beta|rc)' | head -n 1)"
	@echo "aws provider: $(shell make latest-aws)"

update-terra:
	@tgenv list-remote | head -n 1 > .terragrunt-version
	@tgenv install
	@tfenv list-remote | grep -E -v '(alpha|beta|rc)' | head -n 1 > .terraform-version
	@tfenv install

find-unused-variables:
	@for VARBL in $(shell grep -r -E '^variable' terraform/ --exclude _defaults.tf | awk -F '"' '{print $$2}' | sort | uniq); do \
		grep -q -r "var.$$VARBL" terraform/; \
		if [[ $$? -ne 0 ]]; then grep -E -r "^variable \"$$VARBL\"" terraform/; fi \
	done

tfsec:
	@printf "=====\ntfsec\n=====\n"
	@tfsec --force-all-dirs terraform --config-file .tfsec.yaml
	@rm -rf .tfsec/

test: tfsec lint fmt

drift:
	@AWS_PROFILE=$(PROFILE) driftctl scan --only-unmanaged \
	$(shell aws --profile $(PROFILE) s3 ls --recursive "s3://$(STATE_BUCKET)" | awk '/\.tfstate/ {print "--from tfstate+s3://$(STATE_BUCKET)/"$$NF" "}') \
	--tf-provider-version $(shell make latest-aws)
