resource "aws_iam_user" "lego" {
  name = "lego"
}

resource "aws_iam_user_policy" "lego_validation" {
  name   = "lego_validation"
  user   = aws_iam_user.lego.name
  policy = data.aws_iam_policy_document.lego_validation.json
}

data "aws_iam_policy_document" "lego_validation" {
  statement {
    sid = "UpdateRecord"

    actions = [
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets",
    ]

    resources = [
      aws_route53_zone.main.arn,
    ]
  }

  statement { #tfsec:ignore:aws-iam-no-policy-wildcards
    sid = "GetChange"

    actions = [
      "route53:GetChange",
    ]

    resources = [
      "arn:aws:route53:::change/*",
    ]
  }

  statement {
    sid = "ListZones"

    actions = [
      "route53:ListHostedZonesByName",
    ]

    resources = ["*"]
  }
}
