
module "gitlab_oidc" {
  source            = "../..//modules/configure-openid-connect-in-aws"
  gitlab_url        = "https://gitlab.com"
  aud_value         = "https://gitlab.com"
  match_field       = "sub"
  attached_policies = ["arn:aws:iam::aws:policy/AdministratorAccess"]
  match_value = flatten([for repo, config in local.repos : [for branch in lookup(config, "branches", ["main"]) :
    "project_path:${var.gitlab_group}/${repo}:ref_type:${lookup(config, "ref_type", "branch")}:ref:${branch}"
  ]])
}

output "gitlab_role_arn" {
  value = module.gitlab_oidc.ROLE_ARN
}

locals {
  repos = {
    "tbnj-terraform" = {
      branches = ["master"]
    }
  }
}

variable "gitlab_group" {
  default = "tbell83"
}
