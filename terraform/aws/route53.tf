resource "aws_route53_zone" "main" {
  name = var.domain
}

resource "aws_route53_record" "home" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "home.tombell.io"
  type    = "A"
  ttl     = 300

  records = [
    "173.75.244.249"
  ]

  # this is monitored and managed remotely and subject to change
  lifecycle {
    ignore_changes = [
      records
    ]
  }
}

resource "aws_route53_record" "a" {
  for_each = local.a_records
  zone_id  = aws_route53_zone.main.zone_id
  name     = "${each.key == "_apex" ? "" : "${each.key}."}${aws_route53_zone.main.name}"
  type     = "A"
  ttl      = 300
  records  = each.value
}

resource "aws_route53_record" "cname" {
  for_each = local.cname_records
  zone_id  = aws_route53_zone.main.zone_id
  name     = "${each.key == "_apex" ? "" : "${each.key}."}${aws_route53_zone.main.name}"
  type     = "CNAME"
  ttl      = 300
  records  = each.value
}

resource "aws_route53_record" "txt" {
  for_each = local.txt_records
  zone_id  = aws_route53_zone.main.zone_id
  name     = "${each.key == "_apex" ? "" : "${each.key}."}${aws_route53_zone.main.name}"
  type     = "TXT"
  ttl      = 300
  records  = each.value
}

resource "aws_route53_record" "mx" {
  for_each = local.mx_records
  zone_id  = aws_route53_zone.main.zone_id
  name     = "${each.key == "_apex" ? "" : "${each.key}."}${aws_route53_zone.main.name}"
  type     = "MX"
  ttl      = 300
  records  = each.value
}

locals {
  a_records = {
    "_apex"        = ["35.185.44.232"]
    "ftp"          = ["75.119.198.126"]
    "ftp.food"     = ["75.119.198.75"]
    "ftp.meals"    = ["75.119.198.231"]
    "ftp.password" = ["75.119.198.126"]
    "ftp.slack"    = ["75.119.198.126"]
    "ftp.wine"     = ["75.119.198.75"]
    "meals"        = ["75.119.198.231"]
    "mysql.wine"   = ["66.33.202.251"]
    "password"     = ["75.119.198.126"]
    "slack"        = ["75.119.198.126"]
    "ssh"          = ["75.119.198.126"]
    "ssh.food"     = ["75.119.198.75"]
    "ssh.meals"    = ["75.119.198.231"]
    "ssh.password" = ["75.119.198.126"]
    "ssh.slack"    = ["75.119.198.126"]
    "ssh.wine"     = ["75.119.198.75"]
    "wine"         = ["75.119.198.75"]
    "www.food"     = ["75.119.198.75"]
    "www.meals"    = ["75.119.198.231"]
    "www.password" = ["75.119.198.126"]
    "www.slack"    = ["75.119.198.126"]
    "www.wine"     = ["75.119.198.75"]
  }
  cname_records = {
    "food" = ["tbell83.gitlab.io."]
    "mail" = ["ghs.googlehosted.com."]
    "www"  = ["tbell83.gitlab.io."]
  }
  txt_records = {
    "_acme-challenge.slack"                = ["LBAs9KysG5UPdd7IHZf3gBm4XgeOnZkZBBhz_dsXj_4"]
    "_acme-challenge.www.slack"            = ["jUHpXSplwLAV_gv4n2S4pVIoK8O4jvBx8WsfD2kzKgk"]
    "_gitlab-pages-verification-code"      = ["gitlab-pages-verification-code=7d8b918abee75a43f0c95aca8b3803e0"]
    "_gitlab-pages-verification-code.food" = ["gitlab-pages-verification-code=d3950e6fb4a1effbe620c4b10013575e"]
    "_gitlab-pages-verification-code.www"  = ["gitlab-pages-verification-code=b60b349c657ebd8132fad1d7faa7176d"]
  }
  mx_records = {
    "_apex" = [
      "1 ASPMX.L.GOOGLE.COM.",
      "5 ALT1.ASPMX.L.GOOGLE.COM.",
      "5 ALT2.ASPMX.L.GOOGLE.COM.",
      "10 ALT3.ASPMX.L.GOOGLE.COM.",
      "10 ALT4.ASPMX.L.GOOGLE.COM.",
    ]
  }
}
