provider "aws" {
  profile = var.aws_profile
  region  = "us-east-1"

  default_tags {
    tags = local.default_tags
  }
}

provider "aws" {
  alias   = "us-west-2"
  region  = "us-west-2"
  profile = var.aws_profile
}

terraform {
  backend "s3" {}

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.74.3"
    }
  }
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

locals {
  default_tags = {
    managed = "Terraform"
    repo    = local.repo
  }
  repo = "${var.git_org}/${var.git_repo}"
}
