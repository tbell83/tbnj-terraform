module "s3_subdomain" {
  source = "../..//modules/s3_subdomain/"

  for_each     = var.s3_subdomains
  route53_zone = aws_route53_zone.main
  log_bucket   = aws_s3_bucket.logging
  sub_domain   = each.key
}
