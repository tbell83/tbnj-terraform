resource "aws_budgets_budget" "cost" {
  name              = "$10/Month"
  budget_type       = "COST"
  limit_amount      = "10.0"
  limit_unit        = "USD"
  time_unit         = "MONTHLY"
  time_period_start = "2020-01-01_00:00"
}
