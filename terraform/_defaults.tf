variable "aws_profile" {}

variable "domain" {}

variable "env" {}

variable "region" {}

variable "environment" {}

variable "git_org" {}

variable "git_repo" {}
