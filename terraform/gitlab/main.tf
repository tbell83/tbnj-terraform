module "projects" {
  source   = "../..//modules/gitlab_project/"
  for_each = local.projects

  name              = each.key
  description       = each.value
  project_variables = local.deployment_vars
}

terraform {
  backend "s3" {}

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.16.0"
    }
  }
}

provider "gitlab" {}

data "terraform_remote_state" "aws" {
  backend = "s3"

  config = {
    bucket       = "${var.env}.terraform.${var.region}"
    key          = "${var.git_repo}/environments/aws/terraform.tfstate"
    profile      = var.aws_profile
    encrypt      = true
    region       = var.region
    session_name = "terraform"
  }
}
