locals {
  deployment_vars = {
    AWS_DEFAULT_REGION = {
      value     = "us-east-1"
      protected = false
      masked    = false
    }
    ROLE_ARN = {
      value     = data.terraform_remote_state.aws.outputs.gitlab_role_arn
      protected = false
      masked    = false
    }
  }

  projects = {
    "food.tombell.io" = "food codebase",
    "www.tombell.io"  = "www codebase",
  }
}
