resource "aws_route53_record" "sub_domain" {
  for_each = toset(var.is_ipv6_enabled ? ["A", "AAAA"] : ["A"])
  zone_id  = var.route53_zone.zone_id
  name     = local.fqdn
  type     = each.value

  alias {
    name                   = aws_cloudfront_distribution.sub_domain.domain_name
    zone_id                = aws_cloudfront_distribution.sub_domain.hosted_zone_id
    evaluate_target_health = false
  }
}
