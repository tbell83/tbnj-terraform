variable "route53_zone" {
  description = "Route53 zone object"
}

variable "log_bucket" {
  description = "S3 object for log bucket"
}

variable "sub_domain" {
  type        = string
  description = "Sub domain"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Tags for applicable resources"
}

variable "is_ipv6_enabled" {
  type        = bool
  default     = true
  description = "(Optional) - Whether the IPv6 is enabled for the distribution."
}
