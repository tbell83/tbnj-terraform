output "cloudfront_distribution" {
  value       = aws_cloudfront_distribution.sub_domain
  description = ""
}

output "cloudfront_origin_access_identity" {
  value       = aws_cloudfront_origin_access_identity.sub_domain
  description = ""
}

output "route53_record" {
  value       = aws_route53_record.sub_domain
  description = ""
}

output "bucket" {
  value       = aws_s3_bucket.sub_domain
  description = ""
}
