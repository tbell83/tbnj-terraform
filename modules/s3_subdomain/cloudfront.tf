resource "aws_cloudfront_distribution" "sub_domain" {
  enabled             = true
  is_ipv6_enabled     = var.is_ipv6_enabled
  comment             = "Some comment"
  aliases             = [local.fqdn]
  wait_for_deployment = false
  default_root_object = "index.html"

  origin {
    domain_name = aws_s3_bucket.sub_domain.bucket_regional_domain_name
    origin_id   = "s3_${aws_s3_bucket.sub_domain.bucket}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.sub_domain.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "s3_${aws_s3_bucket.sub_domain.bucket}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  logging_config {
    include_cookies = false
    bucket          = var.log_bucket.bucket_domain_name
    prefix          = "cloudfront/${local.fqdn}/"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Environment = "production"
  }

  viewer_certificate {
    acm_certificate_arn      = module.sub_domain.certificate.arn
    minimum_protocol_version = "TLSv1.2_2019"
    ssl_support_method       = "sni-only"
  }
}

resource "aws_cloudfront_origin_access_identity" "sub_domain" {
  comment = local.fqdn
}
