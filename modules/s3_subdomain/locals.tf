locals {
  domain = var.route53_zone.name
  fqdn   = join(".", compact(split(".", "${var.sub_domain}.${local.domain}")))
  tags = merge({
    Module = "s3_subdomain"
  }, var.tags)
}
