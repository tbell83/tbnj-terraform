resource "aws_s3_bucket" "sub_domain" {
  bucket = local.fqdn
  tags   = local.tags

  logging {
    target_bucket = var.log_bucket.id
    target_prefix = "s3/${local.fqdn}/"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_policy" "sub_domain" {
  bucket = aws_s3_bucket.sub_domain.bucket
  policy = data.aws_iam_policy_document.sub_domain.json
}

data "aws_iam_policy_document" "sub_domain" {
  statement {
    sid = "AllowCloudFront"

    actions = [
      "s3:GetObject"
    ]

    resources = [
      "${aws_s3_bucket.sub_domain.arn}/*"
    ]

    principals {
      type = "AWS"
      identifiers = [
        aws_cloudfront_origin_access_identity.sub_domain.iam_arn
      ]
    }
  }
}

resource "aws_s3_bucket_public_access_block" "sub_domain" {
  bucket = aws_s3_bucket.sub_domain.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
