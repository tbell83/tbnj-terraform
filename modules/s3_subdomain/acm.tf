module "sub_domain" {
  source = "../acm_certificate"

  domain_name  = local.fqdn
  route53_zone = var.route53_zone
}
