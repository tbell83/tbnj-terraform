variable "project_variables" {
  type        = map(map(string))
  description = "Project variables"
}

variable "description" {
  type        = string
  description = "Project description"
}

variable "visibility_level" {
  type        = string
  description = "Project visibility level"
  default     = "public"
}

variable "default_branch" {
  type        = string
  description = "Project default branch"
  default     = "master"
}

variable "remove_source_branch_after_merge" {
  type    = bool
  default = true
}

variable "name" {
  type        = string
  description = "Project name."
}
