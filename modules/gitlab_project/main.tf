resource "gitlab_project" "project" {
  name                             = var.name
  description                      = var.description
  visibility_level                 = var.visibility_level
  default_branch                   = var.default_branch
  remove_source_branch_after_merge = var.remove_source_branch_after_merge
}

resource "gitlab_project_variable" "variable" {
  for_each = var.project_variables

  project   = gitlab_project.project.id
  key       = each.key
  value     = each.value.value
  protected = each.value.protected
  masked    = each.value.masked
}

terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}
