# Configure OpenID Connect (OIDC) between AWS and GitLab


## Use-cases
* Retrieve temporary credentials from AWS to access cloud services
* Use credentials to retrieve secrets or deploy to an environment
* Scope role to branch or project

For additional details, see [documentation here](https://docs.gitlab.com/ee/ci/cloud_services/aws/)


## Steps
1. Create an IAM policy with service permissions in AWS. This example below will allow me to fetch a list of s3 buckets in the CI/CD pipeline

   ```json
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:ListBucket"
                ],
                "Resource": "*"
            }
        ]
    }
   ```

2. Add a `terraform.tfvars` file with your gitlab_url, aud value, and the role to assume (Policy ARN created above)
    This example would allow any project in the `mygroup` group running a job for the `main` branch. 
    ```
    aws_region      = "eu-west-2"
    gitlab_url      = "https://gitlab.example.com"
    aud_value       = "https://gitlab.example.com"
    match_field     = "sub"
    match_value     = ["project_path:mygroup/*:ref_type:branch:ref:main"]
    assume_role_arn = ["arn:aws:iam::9999:policy/name_of_policy"]
    ```

4.  Run terrorm init, plan, and apply.
5.  Set the ROLE_ARN variable in the GitLab Project (Settings->CI). This value is generated as output after `terraform apply` is run.
6.  Utilize `$CI_JOB_JWT_V2` in the pipeline to retrieve credentials from AWS STS
    ```yaml
    assume role:
        script:
            - >
            STS=($(aws sts assume-role-with-web-identity
            --role-arn ${ROLE_ARN}
            --role-session-name "GitLabRunner-${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}"
            --web-identity-token $CI_JOB_JWT_V2
            --duration-seconds 3600
            --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]'
            --output text))
            - export AWS_ACCESS_KEY_ID="${STS[0]}"
            - export AWS_SECRET_ACCESS_KEY="${STS[1]}"
            - export AWS_SESSION_TOKEN="${STS[2]}"
            - aws sts get-caller-identity

    ```

## Resources

- https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_oidc.html 
- https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRoleWithWebIdentity.html 
