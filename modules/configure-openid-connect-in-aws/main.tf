data "tls_certificate" "gitlab" {
  url = var.gitlab_url
}

resource "aws_iam_openid_connect_provider" "gitlab" {
  url             = var.gitlab_url
  client_id_list  = [var.aud_value]
  thumbprint_list = [data.tls_certificate.gitlab.certificates.0.sha1_fingerprint]
}

data "aws_iam_policy_document" "assume-role-policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.gitlab.arn]
    }
    condition {
      test     = "StringEquals"
      variable = "${aws_iam_openid_connect_provider.gitlab.url}:${var.match_field}"
      values   = var.match_value
    }
  }
}

resource "aws_iam_role" "gitlab_ci" {
  name               = "GitLabCI"
  assume_role_policy = data.aws_iam_policy_document.assume-role-policy.json
}

resource "aws_iam_role_policy_attachment" "attached_policies" {
  for_each   = var.attached_policies
  role       = aws_iam_role.gitlab_ci.name
  policy_arn = each.value
}

output "gitlab_role" {
  description = "Gitlab runner role"
  value       = aws_iam_role.gitlab_ci
}

output "ROLE_ARN" {
  description = "ARN of the role that needs to be assumed by GitLab CI"
  value       = aws_iam_role.gitlab_ci.arn
}
