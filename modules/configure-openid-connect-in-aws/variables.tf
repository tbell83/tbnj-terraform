
variable "aws_region" {
  type    = string
  default = "us-east-1"
}

variable "gitlab_url" {
  type    = string
  default = "https://gitlab.com"
}

variable "aud_value" {
  type    = string
  default = "https://gitlab.com"
}
variable "match_field" {
  type    = string
  default = "aud"
}

variable "match_value" {
  type = list(any)
}

variable "attached_policies" {
  type = set(any)
}
