variable "tags" {
  type = map(string)

  default = {
    Managed = "acm_certificate"
  }
}

variable "domain_name" {
  type        = string
  description = "A domain name for which the certificate should be issued"
}

variable "route53_zone" {
  description = "Route53 zone object"
}

variable "subject_alternative_names" {
  type        = list(string)
  description = "A list of domains that should be SANs in the issued certificate"
  default     = []
}

variable "certificate_transparency_logging_preference" {
  type        = string
  default     = "ENABLED"
  description = "Specifies whether certificate details should be added to a certificate transparency log. Valid values are ENABLED or DISABLED. See https://docs.aws.amazon.com/acm/latest/userguide/acm-concepts.html#concept-transparency for more details."
}
